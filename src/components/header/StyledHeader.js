import styled from 'styled-components';
import { colors } from '../stylesCommon/theme.js';

export const StyledHeader = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-around;

  img {
    width: 200px;
    height: 200px;
    border-radius: 50%;
  }

  section {
    color: white;
    background-color: ${colors.primary};
    padding: 1em 1.2em;
    border-radius: 50%;
    font-size: 1.2em;
  }
`;
