import React from 'react';
import { StyledLink } from '../stylesCommon/styledLink.js';
import { StyledHeader } from './StyledHeader.js';
import { StyledNavigation } from '../stylesCommon/styledNavigation.js';
import { Link, useLocation } from 'react-router-dom';

function Header({ image, phonesSelected }) {
  const location = useLocation();

  const breadCrumb = location.pathname.split('/')[1];
  const breadCrumbProduct = location.pathname.split('/')[2];

  const currentLink = `${breadCrumb}/${breadCrumbProduct}`;

  return (
    <StyledHeader>
      <Link to='/'>
        <img src={image} alt='phone store logo' title='phone store logo' />
      </Link>
      <StyledNavigation>
        <StyledLink
          to='/'
          variant={location.pathname === '/' ? 'bold' : 'regular'}
        >
          Home
        </StyledLink>
        <StyledLink
          to={currentLink}
          variant={location.pathname === '/' ? 'regular' : 'bold'}
        >
          {breadCrumb}
        </StyledLink>
      </StyledNavigation>
      <section>{phonesSelected}</section>
    </StyledHeader>
  );
}

export default Header;
