import React from 'react';
import { StyledDescription } from './StyledDescription.js';

function Description ({ product }) {
  return (
    <>
      <StyledDescription>
        <li aria-label='Brand'>
          <span>Brand: </span>
          {product.brand}
        </li>
        <li aria-label='Model'>
          <span>Model: </span>
          {product.model}
        </li>
        <li aria-label='Price'>
          <span>Price: </span>
          {product.price}€
        </li>
        <li aria-label='CPU'>
          <span>CPU: </span>
          {product.cpu}
        </li>
        <li aria-label='RAM'>
          <span>RAM: </span>
          {product.ram}
        </li>
        <li aria-label='OS'>
          <span>OS: </span>
          {product.os}
        </li>
        <li aria-label='Display Resolution'>
          <span>Display Resolution: </span>
          {product.displayResolution}
        </li>
        <li aria-label='Battery'>
          <span>Battery: </span>
          {product.battery}
        </li>
        <li aria-label='Camera'>
          <span>Camera: </span>
          {product.primaryCamera}
        </li>
        <li aria-label='Dimensions'>
          <span>Dimensions: </span>
          {product.dimensions}
        </li>
        <li aria-label='Weight'>
          <span>Weight: </span>
          {product.weight}
        </li>
      </StyledDescription>
    </>
  );
}

export default Description;
