import React from 'react';
import { render, screen } from '@testing-library/react';
import Description from './description';
import '@testing-library/jest-dom/extend-expect';

describe('Description component', () => {
  const product = {
    brand: 'Acer',
    model: 'Iconia Talk S',
    price: 199,
    cpu: 'Quad-core 1.3 GHz',
    ram: '2 GB',
    os: 'Android 6.0 (Marshmallow)',
    displayResolution: '720 x 1280 pixels',
    battery: 'Li-Ion 3400 mAh battery',
    primaryCamera: '13 MP',
    dimensions: '191.7 x 101 x 9.4 mm',
    weight: '260 g'
  };

  it('renders product information correctly', () => {
    render(<Description product={product} />);

    expect(
      screen.getByRole('listitem', { name: /brand/i })
    ).toBeInTheDocument();
    expect(
      screen.getByRole('listitem', { name: /model/i })
    ).toBeInTheDocument();
    expect(
      screen.getByRole('listitem', { name: /price/i })
    ).toBeInTheDocument();
    expect(screen.getByRole('listitem', { name: /cpu/i })).toBeInTheDocument();
    expect(screen.getByRole('listitem', { name: /ram/i })).toBeInTheDocument();
    expect(screen.getByRole('listitem', { name: /os/i })).toBeInTheDocument();
    expect(
      screen.getByRole('listitem', { name: /display resolution/i })
    ).toBeInTheDocument();
    expect(
      screen.getByRole('listitem', { name: /battery/i })
    ).toBeInTheDocument();
    expect(
      screen.getByRole('listitem', { name: /camera/i })
    ).toBeInTheDocument();
    expect(
      screen.getByRole('listitem', { name: /dimensions/i })
    ).toBeInTheDocument();
    expect(
      screen.getByRole('listitem', { name: /weight/i })
    ).toBeInTheDocument();
  });
});
