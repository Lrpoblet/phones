import styled from 'styled-components';
import { colors } from '../../../stylesCommon/theme.js';

export const StyledDescription = styled.ul`
  margin: 1em;
  text-align: left;

  li {
    margin: 0.5em;
  }

  span {
    font-weight: bold;
    color: ${colors.primary};
  }
`;
