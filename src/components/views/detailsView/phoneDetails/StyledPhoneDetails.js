import styled from 'styled-components';

export const StyledPhoneDetails = styled.section`
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;

  section {
    display: flex;
    flex-direction: column;
  }
`;
