import React from 'react';
import Image from '../../../image/image.js';
import Description from '../description/description.js';
import Actions from '../actions/actions.js';
import { StyledPhoneDetails } from './StyledPhoneDetails.js';

const PhoneDetails = ({ product, postProduct }) => {
  return (
    <StyledPhoneDetails>
      <Image product={product} />
      <section>
        <Description product={product} />
        <Actions product={product} postProduct={postProduct} />
      </section>
    </StyledPhoneDetails>
  );
};

export default PhoneDetails;
