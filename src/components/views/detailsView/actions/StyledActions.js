import styled from 'styled-components';
import { colors } from '../../../stylesCommon/theme.js';

export const StyledActions = styled.section`
  label {
    margin: 1em;
  }

  select {
    padding: 0.5em;
    cursor: pointer;
    border-radius: 5px;
  }

  option {
    color: ${colors.primary};
  }

  button {
    background-color: ${colors.primary};
    color: white;
    padding: 0.5em;
    cursor: pointer;
    margin: 1em;

    &:hover {
      background-color: ${colors.secondary};
    }
  }
`;
