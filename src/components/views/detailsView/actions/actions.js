import React from 'react';
import { StyledActions } from './StyledActions.js';

function Actions ({ product, postProduct }) {
  const colors = product.colors;
  const storages = product.storages;

  return (
    <>
      {product === undefined
        ? (
          <p>Product not found</p>
          )
        : (
          <StyledActions>
            <div>
              <label htmlFor='colors'>Select a color:</label>
              <select id='colors' name='colors'>
                {colors.map((color) => {
                  return (
                    <option key={color.code} value={color.name}>
                      {color.name}
                    </option>
                  );
                })}
              </select>
              <label htmlFor='storages'>Select a storage:</label>
              <select id='storages' name='storages'>
                {storages.map((storage) => {
                  return (
                    <option key={storage.code} value={storage.name}>
                      {storage.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button onClick={postProduct}>Add to shopping cart</button>
          </StyledActions>
          )}
    </>
  );
}

export default Actions;
