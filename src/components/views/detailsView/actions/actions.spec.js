import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Actions from './actions';
import '@testing-library/jest-dom/extend-expect';

describe('Actions', () => {
  it('renders color and storage options', () => {
    const product = {
      colors: [
        {
          code: 1000,
          name: 'blue'
        },
        {
          code: 1001,
          name: 'pink'
        }
      ],
      storages: [
        {
          code: 2000,
          name: '512 MB'
        }
      ]
    };

    render(<Actions product={product} />);

    const colorSelect = screen.getByRole('combobox', {
      name: 'Select a color:'
    });
    const storageSelect = screen.getByRole('combobox', {
      name: 'Select a storage:'
    });

    expect(colorSelect).toBeInTheDocument();
    expect(storageSelect).toBeInTheDocument();

    expect(screen.getByText('blue')).toBeInTheDocument();
    expect(screen.getByText('pink')).toBeInTheDocument();
    expect(screen.getByText('512 MB')).toBeInTheDocument();
  });

  it('calls postProduct function on button click', () => {
    const product = {
      colors: [{ code: 1000, name: 'blue' }],
      storages: [{ code: 2000, name: '512 MB' }]
    };
    const postProductMock = jest.fn();

    render(<Actions product={product} postProduct={postProductMock} />);

    const addButton = screen.getByRole('button', {
      name: 'Add to shopping cart'
    });

    fireEvent.click(addButton);

    expect(postProductMock).toHaveBeenCalledTimes(1);
  });
});
