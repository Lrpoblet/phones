import React from 'react';
import { StyledSearch } from './StyledSearch.js';

function Search ({ handleSubmit, filter, setFilter }) {
  function handleChangeFilter (event) {
    setFilter(event.target.value);
  }

  return (
    <StyledSearch>
      <form onSubmit={handleSubmit}>
        <label htmlFor='searchInput'>Search:</label>
        <input
          type='text'
          id='searchInput'
          placeholder='Search by brand or model'
          onChange={handleChangeFilter}
          value={filter}
        />
      </form>
    </StyledSearch>
  );
}

export default Search;
