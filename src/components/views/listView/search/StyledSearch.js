import styled from 'styled-components';
import { colors } from '../../../stylesCommon/theme.js';

export const StyledSearch = styled.section`
  margin: 1em;
  display: flex;
  justify-content: flex-end;

  label {
    color: ${colors.primary};
    font-size: 1.2em;
    font-weight: bold;
  }

  input {
    margin: 1em;
    padding: 0.2em;
  }
`;
