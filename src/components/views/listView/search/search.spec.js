import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Search from './search';

describe('Search', () => {
  it('renders search input with correct placeholder', () => {
    render(<Search />);
    const inputElement = screen.getByPlaceholderText('Search by brand or model');

    expect(inputElement).toBeInTheDocument();
  });

  it('updates filter value when input value changes', () => {
    const setFilter = jest.fn();
    render(<Search filter='' setFilter={setFilter} />);
    const inputElement = screen.getByPlaceholderText('Search by brand or model');

    fireEvent.change(inputElement, { target: { value: 'Samsung' } });

    expect(setFilter).toHaveBeenCalledWith('Samsung');
  });
});
