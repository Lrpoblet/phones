import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render, screen } from '@testing-library/react';
import Item from './item';
import { MemoryRouter } from 'react-router-dom';
import defaultImage from '../../../../images/phone-placeholder.png';

jest.mock(
  '../../../../images/phone-placeholder.png',
  () => 'phone-placeholder.png'
);

describe('Item', () => {
  const phone = {
    id: 'ZmGrkLRPXOTpxsU4jjAcv',
    brand: 'Acer',
    model: 'Iconia Talk S',
    price: '170',
    imgUrl:
      'https://front-test-api.herokuapp.com/images/ZmGrkLRPXOTpxsU4jjAcv.jpg'
  };

  it('renders the phone brand, model, and price correctly', () => {
    render(
      <MemoryRouter>
        <Item phone={phone} />
      </MemoryRouter>
    );

    const brandElement = screen.getByRole('heading', { name: phone.brand });
    const modelElement = screen.getByText(phone.model);
    const priceElement = screen.getByText(`${phone.price}€`);

    expect(brandElement).toBeInTheDocument();
    expect(modelElement).toBeInTheDocument();
    expect(priceElement).toBeInTheDocument();
  });

  it('renders the link with the correct URL', () => {
    render(
      <MemoryRouter>
        <Item phone={phone} />
      </MemoryRouter>
    );

    const linkElement = screen.getByRole('link');
    expect(linkElement.getAttribute('href')).toBe(`/product/${phone.id}`);
  });

  it('renders default image when imgUrl is not available', () => {
    render(
      <MemoryRouter>
        <Item phone={phone} />
      </MemoryRouter>
    );

    const imageElement = screen.getByAltText(`${phone.brand}: ${phone.model}`);

    fireEvent.error(imageElement);

    expect(imageElement.src).toContain(defaultImage);
  });
});
