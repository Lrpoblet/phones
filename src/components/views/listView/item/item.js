import React from 'react';
import { Link } from 'react-router-dom';
import Image from '../../../image/image.js';

const Item = ({ phone }) => {
  return (
    <li>
      <article>
        <Link to={`/product/${phone.id}`}>
          <Image product={phone} />
          <h2>{phone.brand}</h2>
          <div>
            <p>{phone.model}</p>
            <p>{phone.price}€</p>
          </div>
        </Link>
      </article>
    </li>
  );
};

export default Item;
