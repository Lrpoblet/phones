import React from 'react';
import Item from '../item/item.js';
import { StyledPhoneList } from './StyledPhoneList.js';

const PhoneList = ({ filteredPhones, filter }) => {
  const renderPhones = filteredPhones.map((phone) => {
    return <Item phone={phone} key={phone.id} />;
  });

  const notFound = (
    <div>
      <p>There is no phone that matches the word {filter}.</p>
    </div>
  );

  return (
    <section>
      <StyledPhoneList>
        {renderPhones.length === 0 ? notFound : renderPhones}
      </StyledPhoneList>
    </section>
  );
};

export default PhoneList;
