import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import PhoneList from './phoneList.js';
import { MemoryRouter } from 'react-router-dom';

jest.mock(
  '../../../../images/phone-placeholder.png',
  () => 'phone-placeholder.png'
);

describe('PhoneList', () => {
  it('should render the phone model for each phone in the filteredPhones prop', () => {
    const filteredPhones = [
      { id: 1, model: 'Phone 1' },
      { id: 2, model: 'Phone 2' },
      { id: 3, model: 'Phone 3' }
    ];

    render(
      <MemoryRouter>
        <PhoneList filteredPhones={filteredPhones} />
      </MemoryRouter>
    );

    filteredPhones.forEach((phone) => {
      const phoneModelElement = screen.getByText(phone.model);
      expect(phoneModelElement).toBeInTheDocument();
    });
  });
});
