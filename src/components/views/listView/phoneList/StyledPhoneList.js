import styled from 'styled-components';
import { colors } from '../../../stylesCommon/theme.js';

export const StyledPhoneList = styled.ul`
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  flex-wrap: wrap;

  li {
    flex-basis: 20%;
    padding: 0.5em;
    margin: 1em;
    border-radius: 5%;
    border: 5px ${colors.primary} solid;
  }

  h2 {
    font-weight: bold;
    font-size: 1.2em;
  }

  div {
    display: flex;
    justify-content: center;
  }

  p {
    margin: 0.5em;
  }

  a {
    color: ${colors.primary};
  }
`;
