import React, { useEffect, useState } from 'react';
import '../styles/reset.css';
import { Route, Routes } from 'react-router-dom';
import { callToApi, getProduct, postProduct } from '../services/api.js';
import ls from '../services/localStorage.js';
import image from '../images/phone-store.png';
import Header from './header/header.js';
import PhoneList from './views/listView/phoneList/phoneList.js';
import Search from './views/listView/search/search.js';
import PhoneDetails from './views/detailsView/phoneDetails/phoneDetails.js';

function App() {
  const [count, setCount] = useState(0);
  const [phoneList, setPhoneList] = useState([]);
  const [filter, setFilter] = useState('');
  const [product, setProduct] = useState({});

  useEffect(() => {
    callToApi()
      .then((data) => {
        ls.set('phoneList', data);
        setPhoneList(data);
      })
      .catch((error) => {
        console.error(error);
        const storedData = ls.get('phoneList');
        setPhoneList(storedData);
      });
  }, []);

  useEffect(() => {
    getProduct()
      .then((data) => {
        const {
          brand,
          model,
          price,
          cpu,
          ram,
          os,
          displayResolution,
          battery,
          primaryCamera,
          dimensions,
          weight,
          options,
          imgUrl,
          id
        } = data;

        const { colors, storages } = options;

        const productData = {
          brand,
          model,
          price,
          cpu,
          ram,
          os,
          displayResolution,
          battery,
          primaryCamera,
          dimensions,
          weight,
          colors,
          storages,
          imgUrl,
          id
        };

        setProduct(productData);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  const handlePostProduct = () => {
    const requestData = {
      id: '0001',
      colorCode: 1,
      storageCode: 2
    };

    postProduct(requestData)
      .then((data) => {
        setCount(data.count);
      })
      .catch(() => {
        setCount((prevCount) => prevCount + 1);
      });
  };

  const handleSubmit = (ev) => {
    ev.preventDefault();
  };

  const filteredPhones = () => {
    return phoneList.filter((phone) => {
      return (
        phone.model.toLowerCase().includes(filter.toLowerCase()) ||
        phone.brand.toLowerCase().includes(filter.toLowerCase())
      );
    });
  };

  return (
    <div>
      <Header
        image={image}
        phonesSelected={count}
      />
      <main>
        <Routes>
          <Route
            path='/'
            element={
              <>
                <Search
                  handleSubmit={handleSubmit}
                  filter={filter}
                  setFilter={setFilter}
                />
                <PhoneList filter={filter} filteredPhones={filteredPhones()} />
              </>
            }
          />
          <Route
            path='/product/:id'
            element={
              <PhoneDetails
                product={product}
                postProduct={handlePostProduct}
              />
            }
          />
        </Routes>
      </main>
    </div>
  );
}

export default App;
