import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { colors } from './theme.js';

export const StyledLink = styled(Link)`
  color: ${colors.primary};
  padding: 1em;
  font-weight: ${(props) => (props.variant === 'bold' ? 'bold' : 'regular')};

  &:hover {
    color: ${colors.secondary};
  }
`;
