import React from 'react';
import defaultImage from '../../images/phone-placeholder.png';
import { StyledImage } from './StyledImage.js';

function Image ({ product }) {
  const handleImageError = (event) => {
    event.target.src = defaultImage;
  };
  return (
    <StyledImage
      src={product.imgUrl}
      alt={`${product.brand}: ${product.model}`}
      onError={handleImageError}
    />
  );
}

export default Image;
