import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Image from './image.js';

jest.mock('../../images/phone-placeholder.png', () => 'phone-placeholder.png');

describe('Image component', () => {
  const product = {
    imgUrl: 'https://front-test-api.herokuapp.com/images/ZmGrkLRPXOTpxsU4jjAcv.jpg',
    brand: 'Acer',
    model: 'Iconia Talk S'
  };

  it('renders image with correct src and alt', () => {
    render(<Image product={product} />);

    const imgElement = screen.getByAltText(`${product.brand}: ${product.model}`);
    expect(imgElement).toBeInTheDocument();
    expect(imgElement).toHaveAttribute('src', product.imgUrl);
  });

  it('handles image error and displays default image', () => {
    render(<Image product={product} />);

    const imgElement = screen.getByAltText(`${product.brand}: ${product.model}`);
    fireEvent.error(imgElement);
    expect(imgElement.src).toContain('phone-placeholder.png');
  });
});
