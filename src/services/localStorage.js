import cacheUtils from './cache.js';

const { cacheData, getCachedData } = cacheUtils;

const get = (key, defaultValue) => {
  const cachedData = getCachedData(key, defaultValue);
  return cachedData !== defaultValue ? cachedData : defaultValue;
};

const set = (key, value) => {
  cacheData(key, value);
};

const remove = (key) => {
  localStorage.removeItem(key);
};

const clear = () => {
  localStorage.clear();
};

const objectToExport = {
  get,
  set,
  remove,
  clear,
};

export default objectToExport;
