import cacheUtils from './cache';

const { cacheData, getCachedData } = cacheUtils;

describe('Cache Utils', () => {
  beforeEach(() => {
    localStorage.clear();
  });

  it('should return default value when data is not present', () => {
    const defaultValue = 'default';
    const result = getCachedData('key', defaultValue);
    expect(result).toBe(defaultValue);
  });

  it('should return stored value when data is not expired', () => {
    const value = 'stored value';
    const timestamp = new Date().getTime() - 30 * 60 * 1000; // Valor almacenado hace 30 minutos
    const dataToStore = {
      value,
      timestamp,
    };
    localStorage.setItem('key', JSON.stringify(dataToStore));

    const result = getCachedData('key', 'default');
    expect(result).toBe(value);
  });

  it('should return default value when data is expired', () => {
    const defaultValue = 'default';
    const timestamp = new Date().getTime() - 2 * 60 * 60 * 1000; // Valor almacenado hace 2 horas
    const dataToStore = {
      value: 'expired value',
      timestamp,
    };
    localStorage.setItem('key', JSON.stringify(dataToStore));

    const result = getCachedData('key', defaultValue);
    expect(result).toBe(defaultValue);
  });

  it('should store value with current timestamp', () => {
    const value = 'new value';
    cacheData('key', value);

    const storedData = localStorage.getItem('key');
    expect(storedData).not.toBeNull();

    const parsedData = JSON.parse(storedData);
    expect(parsedData.value).toBe(value);

    const currentTime = new Date().getTime();
    const storedTimestamp = parsedData.timestamp;
    expect(currentTime - storedTimestamp).toBeLessThan(1000); // Comprobar que la diferencia sea menor a 1 segundo
  });
});
