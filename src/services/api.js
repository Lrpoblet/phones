import ls from './localStorage.js';
import fallbackData from '../data/products.json';
import productData from '../data/single-product.json';

const callToApi = () => {
  const storedData = ls.get('phoneList');
  if (storedData) {
    return Promise.resolve(storedData);
  } else {
    return Promise.resolve(fallbackData);
  }
};

const getProduct = () => {
  return Promise.resolve(productData);
};

const postProduct = (requestData) => {
  return fetch('https://front-test-api.herokuapp.com/cart', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(requestData)
  })
    .then((response) => response.json())
    .catch((error) => {
      console.error('Error', error);
      throw error;
    });
};

export { callToApi, getProduct, postProduct };
