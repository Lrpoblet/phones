const cacheData = (key, value) => {
  const currentTime = new Date().getTime();
  const dataToStore = {
    value,
    timestamp: currentTime,
  };
  const localStorageData = JSON.stringify(dataToStore);
  localStorage.setItem(key, localStorageData);
};

const getCachedData = (key, defaultValue) => {
  const localStorageData = localStorage.getItem(key);
  if (localStorageData === null) {
    return defaultValue;
  } else {
    const parsedData = JSON.parse(localStorageData);
    const { value, timestamp } = parsedData;

    // Verificar si ha pasado una hora desde la última vez que se guardó el valor
    const expirationTime = 60 * 60 * 1000; // 1 hora en milisegundos
    const currentTime = new Date().getTime();
    if (currentTime - timestamp > expirationTime) {
      // La información ha expirado, se necesita revalidar
      return defaultValue;
    }

    return value;
  }
};

const cacheUtils = {
  cacheData,
  getCachedData,
};

export default cacheUtils;
