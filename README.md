# Phone store

![Phone store logo](./src/images/phone-store.png)

This is a phone store app where users can browse and buy different models of mobile phones. It provides detailed information about each phone, including its make, model, price, and technical features. Users can also search by make or model to find specific phones.

## Features

- Display a list of available phones for purchase.
- Full details of each phone, including brand, model, price, and technical specifications.
- Filtering and search options to find phones by brand or model.
- Ability to add phones to the shopping cart.
- Intuitive navigation through links and route-based navigation.

## Technologies Used

- React: JavaScript library used for building the user interface.
- React Router: Library used for managing the application's routes.
- Styled Components: Library used for styling and component styling.
- Jest and Testing Library: Tools used for unit and integration testing.

## Data Sources

- JSON Data: The application utilizes a JSON file containing phone data to populate the phone listings. This ensures that the application can display phone information even if the backend server or API (https://front-test-api.herokuapp.com/) is unavailable. The JSON file is included in the project's data folder.

- Default Image Placeholder: To handle cases where the image URLs are not available or fail to load, the application uses a default image placeholder. This ensures that the application can still render the phone listings without interruption, providing a seamless user experience.

## Other links of interest

- Favicon: https://icons8.com/

## Getting Started

### Installation

1. Clone this repository to your local machine:

`git clone https://gitlab.com/Lrpoblet/phones`

2. Navigate to the project directory.

3. Run the following command to install the dependencies:

`npm install`

### Usage

To run the app you just need to use the following command:

`npm run start`

To run the tests:

`npm run test`

## Author

Lara R. Poblet - [@lrpoblet](https://lrpoblet.github.io/lara-ramos-poblet/)
